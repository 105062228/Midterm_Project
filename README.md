# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. user page (點一下右上角的信箱)
    2. pos page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. 信箱註冊要經過認證
    2. 無法在內容中寫任何html格式
    3. 可以按讚
    4. singin畫面有背景動畫
    5. 可以幫自己取暱稱跟放大頭貼

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N| 
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|



## Website Detail Description
    登入後如果沒有經過信箱認證將視同沒有登入，可以在主頁面上面看到不同版，主頁面是boostrap的template，目前只有美食版可以用(娛樂版可以當作聊天區)，進入後就可以發表貼文和按讚，可
以任選一篇貼文進入、留言，在貼文發表頁面submit，別人電腦會收到google通知(不管是否有開著網頁)，點擊後可以直接進入論壇，在留言區留言，別人電腦也會收到google通知，點擊後會直接，跳到
留言區，看誰留言。
    貼文跟留言都有紀錄時間，方便使用者知道文章發布時間，有時效性，背景動畫方面是運用canvas實作。
    profile裡面可以更改目前頭像跟名字，下面會顯示該使用者的所有歷史貼文跟讚數。
    按title可以回主頁。

## Security Report (Optional)
    由於經過信箱認證，使用者就不能隨便創建不存在的信箱，就像是用其他網站增強自己賺的安全性。此外使用者無法再貼文中刷html的東西，如此一來，就不會有人幫你家背景音樂，造成吵雜的問題......。
以上就是我的安全性部分。
