function init(){

    var txtEmail=document.getElementById("inputEmail");
    var txtPassword=document.getElementById("inputPassword");
    var btnSignUp=document.getElementById("btnSignUp");
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');

    btnSignUp.addEventListener("click",function(e){
        var email=txtEmail.value;
        var password=txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email,password).then(function(e){
            create_alert("success","congratulation");
            txtEmail.value = "";
            txtPassword.value = "";
        }).catch(function(e){
            var errorMSG=e.message;
            create_alert("error",errorMSG);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });


    btnLogin.addEventListener("click",function(e){
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(e){
            window.location = "index.html";
        })
        .catch(function(e) {
            var errorMsg = e.message;
            create_alert("error", errorMsg);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    var provider=new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener("click",function(e){
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "index.html";
        }).catch(function (e) {
            var errorMsg = e.message;
            create_alert("error", errorMsg);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    var provider_facebook = new firebase.auth.FacebookAuthProvider();
    btnFacebook.addEventListener("click",function(e){
        firebase.auth().signInWithPopup(provider_facebook).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "index.html";
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error", errorMessage);
            var email = error.email;
            var credential = error.credential;
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });


    // Custom alert
    function create_alert(type, message) {
        var alertarea = document.getElementById('custom-alert');
        if (type == "success") {
            str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        } else if (type == "error") {
            str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        }
    }



}

window.onload=function(){
    init();
}