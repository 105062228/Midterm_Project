var x=location.search;
var y=x.split("?");

firebase.auth().onAuthStateChanged(function(user){
    var state=document.getElementById("state");
    if(user){
        if(user.emailVerified){
            user_email=user.email;
            state.innerHTML="<span class='btn btn-sm btn-outline-secondary email' id='profile'>" + user.email + "</span><span class='btn btn-sm btn-outline-secondary email' id='log_out'>Logout</span>";
            var profile=document.getElementById("profile");
            profile.addEventListener("click",function(e){
                window.location="profile.html";
            });
            var log_out=document.getElementById("log_out");
            log_out.addEventListener("click",function(e){
                firebase.auth().signOut().then(function(e){
                    alert("success");
                }).catch(function(e){
                    alert("error");
                });
            });
            post_btn = document.getElementById('post_btn');
            post_txt = document.getElementById('comment');

            //使用者介面
            var name_used="";
            var pic_used="";
            firebase.database().ref('data').once('value').then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var childData=childSnapshot.val();
                    //console.log(childSnapshot.key);
                    let stamp=firebase.auth().currentUser.email.replace(/\./g,"-");
                    if(childSnapshot.key==stamp){
                        name_used=childData.name;
                        pic_used=childData.pic;
                    }
                });
            });

            // The html code for post
            //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='https://pic.pimg.tw/s3162331623/1457486738-4232084055.jpg' alt='哈姆太郎' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_before_username_1_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
            var str_before_username_1_1_ex = "<div class='my-3 p-3 bg rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
            var str_before_username_1_2 = " updates</h6><div class='media text-dark pt-3'><img src='";
            var str_before_username_2 = "' alt='去profile加圖片喔' class='mr-2 rounded' style='height:32px; width:48px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>\n";

            var postsRef = firebase.database().ref('discuss/'+y[1]);
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;

            var temp=0;
            var amp=[];


            firebase.database().ref('forum_1').once('value').then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var childData=childSnapshot.val();
                    var date_used;
                    date_used=(childData.time).split(" ");
                    var date;
                    date=date_used[3]+'/'+date_used[1]+'/'+date_used[2];
                    var plugin = "<span class='theme'>" + childData.theme + "</span>";
                    if(childData.id==y[1]){
                        document.getElementById('title').innerHTML = str_before_username_1_1_ex + plugin + ' ' + date + str_before_username_1_2 + childData.pic_before + str_before_username_2 +
                        childData.email + "</strong>" + childData.data_before +childData.data +"<br>"+ childData.time + "<br>"+
                        str_after_content;
                    }
                });
            });

            
            post_btn.addEventListener('click', function () {
                var d=new Date();
                if (post_txt.value != "") {
                    let timestamp=Math.floor(Date.now()/1000);
                    firebase.database().ref('discuss/'+y[1]).child(timestamp).set({
                        email: firebase.auth().currentUser.email,
                        data_before:"<strong>君の名は: "+name_used+"</strong>"+"<br>",
                        pic_before:pic_used,
                        data:post_txt.value,
                        time: d.toString()
                    }).catch(e=>console.log(e.message));
                    post_txt.value="";
                }
            });



            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childSnapshot){
                        var childData = childSnapshot.val();
                        //console.log(childData.data);
                        var temp2=temp.toString();
                        var date_used;
                        date_used=(childData.time).split(" ");
                        var date;
                        date=date_used[3]+'/'+date_used[1]+'/'+date_used[2];
                        total_post[total_post.length] = str_before_username_1_1 + date + str_before_username_1_2 + childData.pic_before + str_before_username_2 +
                        childData.email + "</strong>" + childData.data_before +/*childData.data*/"<span id="+temp2+"></span>" + "<br>" + childData.time + "<br>"+
                        str_after_content;
                        first_count += 1;
                        //console.log(document.getElementById("temp2"));
                        amp[amp.length]=childData.data;
                        temp+=1;
                    });
                    document.getElementById('post_list').innerHTML = 
                    total_post.join('');
                    for(a=0;a<temp;a++){
                        var temp2=a.toString();
                        document.getElementById(temp2).innerText=amp[a];
                        //console.log("fuck");
                    }

                    postsRef.on('child_added',function(data){
                        second_count += 1;
                        if(second_count > first_count){
                            var temp2=temp.toString();
                            var childData = data.val();
                            var date_used;
                            date_used=(childData.time).split(" ");
                            var date;
                            date=date_used[3]+'/'+date_used[1]+'/'+date_used[2];
                            total_post[total_post.length] = str_before_username_1_1 + date + str_before_username_1_2 + childData.pic_before + str_before_username_2 +
                            childData.email +"</strong>"+ childData.data_before + /*childData.data*/"<span id="+temp2+"></span>" + "<br>" + childData.time + "<br>" +
                            str_after_content;
                            amp[amp.length]=childData.data;
                            temp+=1;
                            document.getElementById('post_list').innerHTML =
                            total_post.join('');
                            //console.log(temp2);
                            for(a=0;a<temp;a++){
                                var temp2=a.toString();
                                document.getElementById(temp2).innerText=amp[a];
                                //console.log("fuck");
                            }
                            if(firebase.auth().currentUser.email!=childData.email){
                                notifyMe(y[1]);
                            }
                        }
                    });
                })
                .catch(e => console.log(e.message));
        }
        else{
            alert("not verification yet");
            user.sendEmailVerification().then(function() {
                console.log("驗證信已寄出");
            }, function(error) {
                console.log("驗證信錯誤");
            });
        }
    }
    else{
        state.innerHTML="<a class='btn btn-sm btn-outline-secondary email' id='sign_in' href='signin.html'>Sign up</a>"
    }
});