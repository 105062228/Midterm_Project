firebase.auth().onAuthStateChanged(function(user){
    var state=document.getElementById("state");
    var id=0;
    if(user){
        if(user.emailVerified){
            user_email=user.email;
            state.innerHTML="<span class='btn btn-sm btn-outline-secondary' id='profile'>" + user.email + "</span><span class='btn btn-sm btn-outline-secondary' id='log_out'>Logout</span>";
            var profile=document.getElementById("profile");
            profile.addEventListener("click",function(e){
                window.location="profile.html";
            });
            var log_out=document.getElementById("log_out");
            log_out.addEventListener("click",function(e){
                firebase.auth().signOut().then(function(e){
                    alert("success");
                    window.location="signin.html"
                }).catch(function(e){
                    alert("error");
                });
            });
            post_btn = document.getElementById('post_btn');
            post_txt = document.getElementById('comment');
            post_theme = document.getElementById('theme');

            var name_used="";
            var pic_used="";
            firebase.database().ref('data').once('value').then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    var childData=childSnapshot.val();
                    //console.log(childSnapshot.key);
                    let stamp=firebase.auth().currentUser.email.replace(/\./g,"-");
                    if(childSnapshot.key==stamp){
                        name_used=childData.name;
                        pic_used=childData.pic;
                    }
                });
            });
            
            post_btn.addEventListener('click', function () {
                if(post_txt.value==""||post_theme.value==""){
                    $.notify("主題跟內容都要填喔!!",{
                        className:"warn",
                        clickToHide: true,
                        autoHide: false
                    });
                }
                var d=new Date();
                var ss=0;
                if(post_txt.value!=""&&post_theme.value!=""){
                    firebase.database().ref('id').once('value').then(function(snapshot){
                        //console.log(snapshot.val().id);
                        if(!snapshot.exists()){
                            firebase.database().ref("id").set({
                                id:0
                            }).catch(e=>console.log(e.message));
                        }
                        else{
                            firebase.database().ref("id").set({
                                id:snapshot.val().id+1
                            }).catch(e=>console.log(e.message));
                            ss=snapshot.val().id+1;
                            console.log(ss);
                        }
                    });
                }

                setTimeout(function(){
                    if (post_txt.value != ""&& post_theme.value!="") {
                        let timestamp=Math.floor(Date.now()/1000);
                        firebase.database().ref("forum_2").child(timestamp).set({
                            email: firebase.auth().currentUser.email,
                            data_before:"<strong>君の名は: "+name_used+"</strong>"+"<br>",
                            pic_before:pic_used,
                            theme:theme.value,
                            data:post_txt.value,
                            id:ss,
                            time: d.toString()
                        }).catch(e=>console.log(e.message));
                        firebase.database().ref("favorite").child(ss).set({
                            num:0
                        });
                        post_txt.value="";
                        theme.value="";
                    }
                }, 800);
            });

            // The html code for post
            //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='https://pic.pimg.tw/s3162331623/1457486738-4232084055.jpg' alt='哈姆太郎' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_before_username_1_1 = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
            var str_before_username_1_2 = " updates</h6><div class='media text-muted pt-3'><img src='";
            var str_before_username_2 = "' alt='去profile加圖片喔' class='mr-2 rounded' style='height:32px; width:48px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>\n";

            var postsRef = firebase.database().ref('forum_2');
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;

            var temp=0;
            var amp=[];
            var total_like=[];

            firebase.database().ref('favorite').once('value').then(function(snapshot){
                snapshot.forEach(function(childSnapshot){
                    childData=childSnapshot.val();
                    total_like[total_like.length]=childData.num;
                });
            });

            var qq=0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childSnapshot){
                        var childData = childSnapshot.val();
                        //console.log(childData.data);
                        var temp2=temp.toString();
                        var date_used;
                        date_used=(childData.time).split(" ");
                        var date;
                        date=date_used[3]+'/'+date_used[1]+'/'+date_used[2];
                        var plugin = "<span class='theme'>" + childData.theme + "</span>";
                        total_post[total_post.length] = str_before_username_1_1 + plugin + ' ' + date + str_before_username_1_2 + childData.pic_before + str_before_username_2 +
                        childData.email + "</strong>" + childData.data_before +/*childData.data*/"<span id="+temp2+"></span>" + "<br>" + childData.time  +
                        str_after_content;
                        qq+=1;
                        first_count += 1;
                        //console.log(document.getElementById("temp2"));
                        amp[amp.length]=childData.data;
                        temp+=1;
                    });
                    document.getElementById('post_list').innerHTML = 
                    total_post.join('');
                    for(a=0;a<temp;a++){
                        var temp2=a.toString();
                        document.getElementById(temp2).innerText=amp[a];
                        //console.log("fuck");
                    }

                    postsRef.on('child_added',function(data){
                        second_count += 1;
                        if(second_count > first_count){
                            var temp2=temp.toString();
                            var childData = data.val();
                            var date_used;
                            date_used=(childData.time).split(" ");
                            var date;
                            var plugin = "<span class='theme'>" + childData.theme + "</span>";
                            date=date_used[3]+'/'+date_used[1]+'/'+date_used[2];
                            total_post[total_post.length] = str_before_username_1_1 + plugin + ' ' + date + str_before_username_1_2 + childData.pic_before + str_before_username_2 +
                            childData.email +"</strong>"+ childData.data_before + /*childData.data*/"<span id="+temp2+"></span>" + "<br>" + childData.time +
                            str_after_content;
                            amp[amp.length]=childData.data;
                            temp+=1;
                            document.getElementById('post_list').innerHTML =
                            total_post.join('');
                            //console.log(temp2);
                            for(a=0;a<temp;a++){
                                var temp2=a.toString();
                                document.getElementById(temp2).innerText=amp[a];
                                //console.log("fuck");
                            }
                            $.notify("have new comment",{
                                className:"success",
                                clickToHide: true,
                            });
                        }
                    });
                    /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
                    ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
                    ///         2. Join all post in list to html in once
                    ///         4. Add listener for update the new post
                    ///         5. Push new post's html to a list
                    ///         6. Re-join all post in list to html when update
                    ///
                    ///         Hint: When history post count is less then new post count, update the new and refresh html
                })
                .catch(e => console.log(e.message));
        }
        else{
            alert("not verification yet");
            user.sendEmailVerification().then(function() {
                console.log("驗證信已寄出");
            }, function(error) {
                console.log("驗證信錯誤");
            });
        }
    }
    else{
        state.innerHTML="<a class='btn btn-sm btn-outline-secondary' id='sign_in' href='signin.html'>Sign up</a>"
    }
});