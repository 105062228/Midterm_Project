//function init_for() {
    //console.log(document.getElementById("sign_in"));

    /*btn_control.addEventListener("click",function(e){
        window.location="signin.html";
    });*/


    firebase.auth().onAuthStateChanged(function(user){
        var state=document.getElementById("state");
        if(user){
            user_email=user.email;
            state.innerHTML="<span class='btn btn-sm btn-outline-secondary'>" + user.email + "</span><span class='btn btn-sm btn-outline-secondary' id='log_out'>Logout</span>";
            var log_out=document.getElementById("log_out");
            log_out.addEventListener("click",function(e){
                firebase.auth().signOut().then(function(e){
                    window.location="index.html";
                    alert("success");
                }).catch(function(e){
                    alert("error");
                });
            });
        }
        else{
            state.innerHTML="<a class='btn btn-sm btn-outline-secondary' id='sign_in' href='signin.html'>Sign up</a>"
        }
    });
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    post_btn.addEventListener('click', function () {
        var d=new Date();
        if (post_txt.value != "") {
            let timestamp=Math.floor(Date.now()/1000);
            firebase.database().ref("forum_3").child(timestamp).set({
                email: firebase.auth().currentUser.email,
                data: post_txt.value,
                time: d.toString(),
                timestamp: timestamp*1000
            }).catch(e=>console.log(e.message));
            post_txt.value="";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='https://pic.pimg.tw/s3162331623/1457486738-4232084055.jpg' alt='哈姆太郎' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('forum_3');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData = childSnapshot.val();
                //console.log(childData.data);
                total_post[total_post.length] = str_before_username + 
                childData.email + "</strong>" + childData.data + "<br>" + childData.time +
                str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = 
            total_post.join('');

            postsRef.on('child_added',function(data){
                second_count += 1;
                if(second_count > first_count){
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username
                    + childData.email + "</strong>" + childData.data + "<br>" + childData.time +
                    str_after_content;
                    document.getElementById('post_list').innerHTML =
                    total_post.join('');
                }
            });
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
        })
        .catch(e => console.log(e.message));

//}

/*window.onload = function () {
    oldonload();
    init_for();
};*/
